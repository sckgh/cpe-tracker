from django.db import models
from django.contrib.auth.models import User


class CertificationBody(models.Model):
    '''
    This class captures certification bodies for CPE.

    This includes:
    *ISACA
    *ISC2
    *Veterinarians Australia

    TODO Add cpe types
    TODO Add API Methods?
    '''
    #Name of the certification organisation
    org_name=models.TextField()
    #URL for the API
    api_url=models.TextField()
    #Listing of industries from ABS (http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/1292.0.55.0022006?OpenDocument)
    industries = (('A','Agriculture, Forestry and Fishing'),
                  ('B','Mining'),
                  ('C','Manufacturing'),
                  ('D','Electricity, Gas, Water and Waste Services'),
                  ('E','Construction'),
                  ('F','Wholesale Trade'),
                  ('G','Retail Trade'),
                  ('H','Accommodation and Food Services'),
                  ('I','Transport, Postal and Warehousing'),
                  ('J','Information Media and Telecommunications'),
                  ('K','Financial and Insurance Services'),
                  ('L','Rental, Hiring and Real Estate Services'),
                  ('M','Professional, Scientific and Technical Services'),
                  ('N','Administrative and Support Services'),
                  ('O','Public Administration and Safety'),
                  ('P','Education and Training'),
                  ('Q','Health Care and Social Assistance'),
                  ('R','Arts and Recreation Services'),
                  ('S','Other Services'))
    #Industry the certification is in -- helps for search
    industry=models.TextField(choices=industries,max_length=1)
    #Reporting date for CPE submission -- an email will be sent to the user at least 3 weeks in advance
    report_date=models.DateTimeField()


class CPERecord(models.Model):
    '''
    This class captures the CPE activity

    TODO: sort S3 for file uploads
    '''
    #unique ID of the CPERecord
    id=models.UUIDField()
    #User who is claiming the CPE
    user= models.OneToOneField(User, on_delete=models.CASCADE)
    #Title of the cpe record
    title=models.TextField(max_length=50)
    #Description of what the record is
    description=models.TextField()
    #Organisations the CPE is aimed at
    eligible_organisation=models.ManyToManyField(CertificationBody)
    start_date=models.DateTimeField()
    end_date=models.DateTimeField()
    #url reference for the activity
    url=models.URLField()
    #First user file for upload
    file_1=models.FileField()
    file_2 = models.FileField()
    file_3 = models.FileField()
    #File downloaded from the URL
    url_file=models.FileField()